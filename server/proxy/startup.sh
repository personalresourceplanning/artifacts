#!/bin/bash

echo "!!! Startup procedure !!!"

discovery_connection_alive=1
auth_connection_alive=1

check_connection() {
  return "$(nc -z $1 $2; echo $?)"
}

perform_connection_checks() {
  echo "Starting connection checks..."
  discovery_connection_alive="$(check_connection "discovery" "8761"; echo $?)"
  auth_connection_alive="$(check_connection "auth" "8081"; echo $?)"
  echo "Discovery state: $discovery_connection_alive :: Auth state: $auth_connection_alive"
}

perform_connection_checks
while [[ $auth_connection_alive != 0 ]] | [[ $discovery_connection_alive != 0 ]]; do
  echo "Connection not alive. Retry in 5 sec."
  sleep 5
  perform_connection_checks
done

echo "Connections alive. Waiting for final initialization of dependencies (30sec)..."
sleep 30

echo "Starting service... Procedure end."

java -jar proxy.jar --eureka.base-url=$DISCOVERY_BASE_URL
